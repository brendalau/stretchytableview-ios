//
//  ViewController.swift
//  StretchyTableView
//
//  Created by Brenda Lau on 09/12/2019.
//  Copyright © 2019 Brenda Lau. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet private weak var tableView: UITableView!

    var reviews: [Review] = []
    var photos: [UIImage] = [#imageLiteral(resourceName: "Flower1"), #imageLiteral(resourceName: "Flower2"), #imageLiteral(resourceName: "Flower3"), #imageLiteral(resourceName: "Flower4") , #imageLiteral(resourceName: "Flower5"), #imageLiteral(resourceName: "Flower6"), #imageLiteral(resourceName: "Flower7") ,#imageLiteral(resourceName: "Flower8"), #imageLiteral(resourceName: "Flower9"), #imageLiteral(resourceName: "Flower10")]

    var headerHeight: CGFloat = 300

    var headerView: HeaderView?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.reviews = Review.testData()
        (self.tableView as UIScrollView).delegate = self

        if let headerView = UINib(nibName: "HeaderView", bundle: nil).instantiate(withOwner: nil, options: [:]).first as? HeaderView {
            headerView.configure(photos: self.photos)
            headerView.frame = CGRect(
                x: 0,
                y: 0,
                width: UIScreen.main.bounds.width,
                height: self.headerHeight
            )
            self.tableView.addSubview(headerView)
            self.headerView = headerView
        }

        self.transparentNavBar()

        self.tableView.contentInset = UIEdgeInsets(top: self.headerHeight, left: 0, bottom: 0, right: 0)
        self.tableView.contentOffset = CGPoint(x: 0, y: -self.headerHeight)
        self.tableView.reloadData()
    }

    private func transparentNavBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
    }

    func updateHeaderView() {
        let yOffset = tableView.contentOffset.y
//        debugPrint(yOffset)
        var headerRect = CGRect(
            x: 0,
            y: -self.headerHeight,
            width: tableView.bounds.width,
            height: headerHeight
        )

        if yOffset < -self.headerHeight {
            headerRect.origin.y = self.tableView.contentOffset.y
            headerRect.size.height = -self.tableView.contentOffset.y
            self.headerView?.imageView?.isHidden = false
        }

        let navBarHeight = self.navigationController?.navigationBar.frame.height ?? 44.0
        let navBarTreshold: CGFloat = -navBarHeight * 2
        if yOffset > navBarTreshold {
            self.navigationController?.navigationBar.tintColor = .blue
        } else {
            self.navigationController?.navigationBar.tintColor = .black
        }

        self.headerView?.frame = headerRect
    }
}

// MARK: - UIScrollViewDelegate
extension ViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.updateHeaderView()
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.headerView?.imageView?.isHidden = true
    }
}

// MARK: - UITableViewDataSource
extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reviews.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let reviewCell = tableView.dequeueReusableCell(withIdentifier: ReviewCell.reuseIdentifier) as? ReviewCell else {
            fatalError()
        }

        reviewCell.configure(self.reviews[indexPath.row])
        return reviewCell
    }
}

