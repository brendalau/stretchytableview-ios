//
//  ReviewCell.swift
//  StretchyTableView
//
//  Created by Brenda Lau on 09/12/2019.
//  Copyright © 2019 Brenda Lau. All rights reserved.
//

import Foundation
import UIKit

public class ReviewCell: UITableViewCell {
    static var reuseIdentifier: String = "ReviewCell"
    @IBOutlet private weak var usernameLabel: UILabel?
    @IBOutlet private weak var descriptionTextView: UITextView?

    func configure(_ review: Review) {
        self.usernameLabel?.text = review.username
        self.descriptionTextView?.text = review.description
    }

    public override func prepareForReuse() {
        super.prepareForReuse()
        self.usernameLabel?.text = nil
        self.descriptionTextView?.text = nil
    }
}
