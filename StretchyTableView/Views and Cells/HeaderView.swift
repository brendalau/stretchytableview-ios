//
//  HeaderView.swift
//  StretchyTableView
//
//  Created by Brenda Lau on 09/12/2019.
//  Copyright © 2019 Brenda Lau. All rights reserved.
//

import Foundation
import UIKit

class HeaderView: UIView {
    @IBOutlet private weak var collectionView: UICollectionView!
    @IBOutlet weak var imageView: UIImageView?

    var photos: [UIImage] = []

    func configure(photos: [UIImage]) {
        self.photos = photos
        self.imageView?.image = photos.first

        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        (self.collectionView as UIScrollView).delegate = self

        self.collectionView.register(UINib(nibName: "PhotoCell", bundle: nil), forCellWithReuseIdentifier: PhotoCell.reuseIdentifier)
        self.collectionView.reloadData()
    }
}

// MARK: -
extension HeaderView: UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let page = Int(scrollView.contentOffset.x / scrollView.frame.size.width)
        self.imageView?.image = self.photos[page]
    }
}

// MARK: - UICollectionViewDataSource
extension HeaderView: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.photos.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let photoCell = collectionView.dequeueReusableCell(withReuseIdentifier: PhotoCell.reuseIdentifier, for: indexPath) as? PhotoCell else {
            fatalError()
        }

        photoCell.configure(self.photos[indexPath.row])
        return photoCell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension HeaderView: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: self.collectionView.bounds.height)
    }
}
