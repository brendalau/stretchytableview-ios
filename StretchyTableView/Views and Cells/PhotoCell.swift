//
//  PhotoCell.swift
//  StretchyTableView
//
//  Created by Brenda Lau on 09/12/2019.
//  Copyright © 2019 Brenda Lau. All rights reserved.
//

import Foundation
import UIKit

public class PhotoCell: UICollectionViewCell {
    static var reuseIdentifier: String = "PhotoCell"

    @IBOutlet private weak var photoImageView: UIImageView?

    func configure(_ photo: UIImage) {
        self.photoImageView?.image = photo
    }

    public override func prepareForReuse() {
        super.prepareForReuse()
        self.photoImageView?.image = nil
    }
}
