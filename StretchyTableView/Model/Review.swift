//
//  Review.swift
//  StretchyTableView
//
//  Created by Brenda Lau on 09/12/2019.
//  Copyright © 2019 Brenda Lau. All rights reserved.
//

import Foundation

public class Review {
    public var username: String
    public var description: String

    init(username: String, description: String) {
        self.username = username
        self.description = description
    }

    static func testData() -> [Review] {
        let review1 = Review(username: "Brenda", description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit")
        let review2 = Review(username: "Chai Kee", description: "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore")
        let review3 = Review(username: "Jason", description: "Velit euismod in pellentesque massa placerat duis ultricies lacus. Sollicitudin aliquam ultrices sagittis orci a.")
        let review4 = Review(username: "Li Theen", description: "Diam sollicitudin tempor id eu nisl. Cursus mattis molestie a iaculis at erat. Imperdiet massa tincidunt nunc pulvinar. Amet est placerat in egestas erat imperdiet sed. Malesuada fames ac turpis egestas sed tempus urna et. Accumsan lacus vel facilisis volutpat est velit egestas dui id.")
        let review5 = Review(username: "Leon", description: " Netus et malesuada fames ac. Mauris pellentesque pulvinar pellentesque habitant morbi tristique senectus et netus. Pellentesque habitant morbi tristique senectus et. Diam quam nulla porttitor massa. Orci sagittis eu volutpat odio facilisis mauris. Tortor vitae purus faucibus ornare. Senectus et netus et malesuada fames. Risus in hendrerit gravida rutrum quisque non.")
        let review6 = Review(username: "Test", description: "Velit euismod in pellentesque massa placerat duis ultricies lacus. Sollicitudin aliquam ultrices sagittis orci a.")
        let review7 = Review(username: "TEST 2", description: "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore")
        return [review1, review2, review3, review4, review5, review6, review7]
    }
}
